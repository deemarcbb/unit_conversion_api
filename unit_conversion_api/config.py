from unit_conversion_api import defaults
import os
# Instantiate config object with defaults
defualt_cfg = {key: getattr(defaults, key) for key in dir(defaults) if key.isupper()}

config = {}
for key,value in defualt_cfg.items():
    #check if config value has been set in enviroment variable or not. if not get from defualt value
    config[key] = os.environ.get(key) or value