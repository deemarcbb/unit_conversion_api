from . import ma
from .models import *
from flask_marshmallow import Marshmallow
from marshmallow import fields, Schema, validates_schema, ValidationError, pre_load, post_load,post_dump,validate,validates

# ======================== Unit section ========================
class UnitSchema(ma.SQLAlchemyAutoSchema):
    type = fields.String(required=True)
    class Meta:
        model = Unit

class UnitSchemaPost(ma.SQLAlchemyAutoSchema):
    type = fields.String(required=True)
    class Meta:
        model = Unit
        exclude = ('id',)

# ======================== Conversion Formula section ========================
class ConversionFormulaSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Conversion
        include_relationships = True
        load_instance = True
    @post_dump
    def change_rel2id(self, data, **kwags):
        if 'x_unit_rel' in data:
            data['x_unit_id'] = data.pop('x_unit_rel')
        if 'y_unit_rel' in data:
            data['y_unit_id'] = data.pop('y_unit_rel')
        return data

class ConversionFormulaSchemaPost(ma.SQLAlchemyAutoSchema):
    # this schemas is current support for linear eqaution
    x_unit_id = fields.Integer(required=True)
    y_unit_id = fields.Integer(required=True)
    a0 = fields.String(required=True)
    a1 = fields.String(required=True)
    class Meta:
        model = Conversion
        include_fk = True
        exclude = ('id',)
 

# ======================== Coversion section ========================

class ConversionSchemaPost(ma.SQLAlchemyAutoSchema):
    x_unit_id = fields.Integer(required=True)
    y_unit_id = fields.Integer(required=True)
    x_value = fields.String(required=True)
    y_value =fields.String(dump_only=True)
