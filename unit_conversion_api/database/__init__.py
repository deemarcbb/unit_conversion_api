from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import contextlib
# from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()




def emptyAllTable():
    try:
        for tbl in reversed(db.metadata.sorted_tables):
            db.session.bind.execute(tbl.delete())
        db.session.commit()
    except:
        db.session.rollback()