
from . import db
from sqlalchemy.ext.associationproxy import association_proxy


class UnitType(db.Model):
    __tablename__  = "unitType"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20),unique=True ,nullable=False)
    unit_rel = db.relationship('Unit', backref='unitType', lazy=True, cascade="all")

class Unit(db.Model):
    __tablename__  = "unit"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20),unique=True ,nullable=False)
    
    unitType_id = db.Column(db.Integer, db.ForeignKey('unitType.id',ondelete='cascade'),nullable=False)
    # conversionFormula_rel = db.relationship('ConversionFormula', backref='unit', lazy=True, cascade="all")
    # unitType_rel = db.relationship('UnitType', backref='unit', lazy=True, cascade="save-update, merge, delete")
    type = association_proxy('unitType', 'name', creator=lambda name: UnitType.query.filter_by(name=name).first() or UnitType(name=name))

class Conversion(db.Model):
    # Currently support only linear equation formula
    # if new equation is required might need to add formula type field
    __tablename__ = "conversion"
    id = db.Column(db.Integer, primary_key=True)
    x_unit_id = db.Column(db.Integer, db.ForeignKey('unit.id',ondelete='cascade'),nullable=False)
    y_unit_id = db.Column(db.Integer, db.ForeignKey('unit.id',ondelete='cascade'),nullable=False)

    x_unit_rel = db.relationship('Unit', backref='conversion_x', foreign_keys=[x_unit_id])
    y_unit_rel = db.relationship('Unit', backref='conversion_y', foreign_keys=[y_unit_id])
  
    a0 = db.Column(db.String(50))
    a1 = db.Column(db.String(50))
    # for linear equation y = a1x+a0