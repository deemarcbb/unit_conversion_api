from functools import partial
from flask import request, abort, jsonify, current_app
from unit_conversion_api.api.v1.masqlapi import masqlapi, loadValidatation
from unit_conversion_api.api.v1 import bp
from unit_conversion_api.database import db
from unit_conversion_api.database.models import Conversion, Unit
from unit_conversion_api.database.schemas import ConversionFormulaSchema, ConversionFormulaSchemaPost
import json
from decimal import Decimal


apiHandle = masqlapi(db.session, Conversion ,ConversionFormulaSchema, ConversionFormulaSchemaPost)

@bp.route('/conversionFormulas', methods=['GET'])
def get_conversionFormula_resource():
    filters = request.args
    # check if query string is valid
    errors = ConversionFormulaSchema().validate(filters, partial=True)
    if errors:
        abort(400, str(errors))

    objs = apiHandle.get_many(**filters)

    return apiHandle.getMethod(objs, many=True)

# @bp.route('/units/<id>', methods=['GET'])
# def get_single_unit_resource(id):
#     obj = apiHandle.get(id=id) or abort(404)
#     return apiHandle.getMethod(obj, many=False)

@bp.route('/conversionFormulas', methods=['POST'])
def post_conversionFormula_resource():
    jsonData= request.get_json()

    # TODO: find a way to include_fk into schema load
    data = loadValidatation(ConversionFormulaSchemaPost, jsonData)
    
    #check if the unit is actually exist
    if not Unit.query.filter_by(id=jsonData["x_unit_id"]).first():
        abort(404, "unit with id:{} doesn't exist".format(jsonData["x_unit_id"]))
    #check if the unit is actually exist
    if not Unit.query.filter_by(id=jsonData["y_unit_id"]).first():
        abort(404, "unit with id:{} doesn't exist".format(jsonData["y_unit_id"]))

    
    # check incase inverse conversion is already exist
    obj = Conversion.query.filter_by(x_unit_id=jsonData["y_unit_id"], y_unit_id=jsonData["x_unit_id"]).first()
    if obj:
        qData = ConversionFormulaSchema().dump(obj)
        return {'message': 'OK - entity exists', 'status_code': 200, 'status': 'success', 'data': qData}

    return apiHandle.postData(["x_unit_id","y_unit_id"],jsonData)

@bp.route('/conversionFormulas/<id>', methods=['PATCH'])
def patch_conversionFormula_resource(id):
    updateUnitPair = False

    obj = apiHandle.get(id=id) or abort(404)

    jsonData= request.get_json()

    # TODO: find a way to include_fk into schema load
    data = loadValidatation(ConversionFormulaSchemaPost, jsonData, partial=True)
    
    #check if the unit is actually exist
    if 'x_unit_id' in jsonData:
        tempUnit_obj = Unit.query.filter_by(id=jsonData["x_unit_id"]).first()
        if not tempUnit_obj:
            abort(409, "unit with id:{} doesn't exist".format(jsonData["x_unit_id"])),409
        x_unit_id = obj.id
        updateUnitPair = True
    else:
        x_unit_id = jsonData["x_unit_id"]
    #check if the unit is actually exist
    if 'y_unit_id' in jsonData:
        tempUnit_obj = Unit.query.filter_by(id=jsonData["y_unit_id"]).first()
        if not tempUnit_obj:
            abort(409, "unit with id:{} doesn't exist".format(jsonData["y_unit_id"])),409
        y_unit_id = jsonData["y_unit_id"]
        updateUnitPair = True
    
    # check incase conversion Formular is already exist
    if updateUnitPair:
        existFormula = Conversion.query.filter_by(x_unit_id=jsonData["x_unit_id"], y_unit_id=jsonData["y_unit_id"]).first()
        if existFormula:
            if (existFormula.id != int(id)):
                print("========================")
                print(f"existFormula.id:{existFormula.id}, id:{id}")
                qData = ConversionFormulaSchema().dump(existFormula)
                return {'message': 'OK - entity exists', 'status_code': 200, 'status': 'success', 'data': qData}
    
        existInvertFormula = Conversion.query.filter_by(x_unit_id=jsonData["y_unit_id"], y_unit_id=jsonData["x_unit_id"]).first()
        if existInvertFormula:
            if (existInvertFormula.id != int(id)):
                qData = ConversionFormulaSchema().dump(existInvertFormula)
                return {'message': 'OK - entity exists', 'status_code': 200, 'status': 'success', 'data': qData}
    
   
    return apiHandle.patchData(obj,jsonData)


@bp.route('/conversionFormulas/<id>', methods=['DELETE'])
def delete_single_conversionFormula_resource(id):
    obj = apiHandle.get(id=id) or abort(404)
    return apiHandle.delete(obj)

