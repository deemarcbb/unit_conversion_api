from functools import partial

from flask import request, abort, jsonify, current_app
from unit_conversion_api.api.v1.masqlapi import masqlapi, loadValidatation
from unit_conversion_api.api.v1 import bp
from unit_conversion_api.database import db
from unit_conversion_api.database.models import Conversion, Unit
from unit_conversion_api.database.schemas import ConversionSchemaPost, ConversionFormulaSchema
import json
from decimal import Decimal


def unitConversion( value, a0, a1, isInvert=False):
    """
    expecting all input parameters to be a string of floating number
    """
    a0 = float(a0)
    a1 = float(a1)
    value = float(value)
    if isInvert:
        return (value-a0)/a1

    return ((a1*value) + a0)

@bp.route('/conversion', methods=['POST'])
def post_conversion_resource():
    jsonData= request.get_json()

    # TODO: find a way to include_fk into schema load
    data = loadValidatation(ConversionSchemaPost, jsonData)
    
    #check if the unit is actually exist
    x_unit = Unit.query.filter_by(id=jsonData["x_unit_id"]).first()
    if not x_unit:
        abort(404, "unit with id:{} doesn't exist".format(jsonData["x_unit_id"]))
    #check if the unit is actually exist
    y_unit = Unit.query.filter_by(id=jsonData["y_unit_id"]).first()
    if not y_unit:
        abort(404, "unit with id:{} doesn't exist".format(jsonData["y_unit_id"]))

    data = {
        "x_unit_id":jsonData["x_unit_id"],
        "y_unit_id":jsonData["x_unit_id"],
        "x_value":jsonData["x_value"],
    }

    # check if the conversion formula is actually exist
    existFormula = Conversion.query.filter_by(x_unit_id=jsonData["x_unit_id"], y_unit_id=jsonData["y_unit_id"]).first()
    if existFormula:
        formulaValue = ConversionFormulaSchema().dump(existFormula)
        data["y_value"] = str(unitConversion(data["x_value"], formulaValue["a0"],formulaValue["a1"]))
        return {'message': None, 'status_code': 200, 'status': 'success', 'data': data}

    existInvertFormula = Conversion.query.filter_by(x_unit_id=jsonData["y_unit_id"], y_unit_id=jsonData["x_unit_id"]).first()
    if existInvertFormula:
        formulaValue = ConversionFormulaSchema().dump(existInvertFormula)
        data["y_value"] = str(unitConversion(data["x_value"], formulaValue["a0"],formulaValue["a1"],isInvert=True))
        return {'message': None, 'status_code': 200, 'status': 'success', 'data': data}

    return abort(404, "Cannot find conversion formular from {} to {}".format(x_unit.name, y_unit.name))