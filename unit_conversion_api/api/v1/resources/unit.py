from flask import request, abort, jsonify, current_app
from unit_conversion_api.api.v1.masqlapi import masqlapi
from unit_conversion_api.api.v1 import bp
from unit_conversion_api.database import db
from unit_conversion_api.database.models import Unit
from unit_conversion_api.database.schemas import UnitSchema, UnitSchemaPost



apiHandle = masqlapi(db.session, Unit ,UnitSchema, UnitSchemaPost)

@bp.route('/units', methods=['GET'])
def get_unit_resource():
    filters = request.args
    # check if query string is valid
    errors = UnitSchema().validate(filters, partial=True)
    if errors:
        abort(400, str(errors))

    objs = apiHandle.get_many(**filters)

    return apiHandle.getMethod(objs, many=True)

@bp.route('/units/<id>', methods=['GET'])
def get_single_unit_resource(id):
    obj = apiHandle.get(id=id) or abort(404)
    return apiHandle.getMethod(obj, many=False)

@bp.route('/units', methods=['POST'])
def post_book_resource():
    return apiHandle.post(["name","type"])

@bp.route('/units/<id>', methods=['PATCH'])
def patch_single_book_resource(id):
    obj = apiHandle.get(id=id) or abort(404)
    return apiHandle.patch(obj)

@bp.route('/units/<id>', methods=['DELETE'])
def delete_single_book_resource(id):
    obj = apiHandle.get(id=id) or abort(404)
    return apiHandle.delete(obj)

