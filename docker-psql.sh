docker run -d \
    --name unit_conversion_api_db \
    -p 127.0.0.1:5442:5432 \
    -e POSTGRES_USER=unit_conversion_api_usr \
    -e POSTGRES_PASSWORD=unit_conversion_api_pass \
    -e POSTGRES_DB=unit_conversion_api_dev \
    postgres