# unit_conversion_api #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A Flask API for convert a value from one unit to another, depending on region and user preferences. For example, a user in Thailand may
prefer the area unit to be in ‘Rai’ (ไร่), but a user in the Philippines would prefer it to be in
‘Square Meter’


### 1. Installing Dependencies

#### Python 3.8
Install the latest version of python for your platform follow this link: [python docs](https://packaging.python.org/guides/installing-using-pip-and-vitual-environments/)
* Remmark this project has been tested to work with python3.8.10

#### Virtual Enviornment
I recommend working within a virtual environment . This keeps your dependencies for each project separate and organaized. Instructions for setting up a virual enviornment for your platform can be found in the python docs
```
python3.8 -m venv [path_to_desired_place]/venv/uniconver
source ./uniconver/bin/activate
```

#### PIP Dependencies
Once you have your virtual environment setup and running, install dependencies if you current directory is where this Readme is, you should be ready to run the [requirement file](requirements.txt)
```
pip install -r requirements.txt
```


##### Key Dependencies
<b>Flask</b> is a lightweight backend microservices framework. Flask is required to handle requests and responses.


#### Install the package
along with this Readme you should see the [setup.py](setup.py) file in the same location

```
pip3 install -e .
```

### 2. Setup postgres database for the service to connect to
if you already have postgres database running and ready to be connect you can just config the service to connect to it

but if not we already provide [the script](docker-psql.sh) to run postgres database inside docker.

please config it before running

```
source docker-psql.sh
```

### 3. Create/Updata database table
As the name state, you only need to run this when you need to create database table or update it (if this is the first time you connect the service to the database the you sure does to run this)

<b>Remarks</b>: you need to run this in the directory where migration folder is located(same location as this readme)

```
[path_to_desired_place]/venv/uniconver/bin/run-manage db upgrade
```

### 4. Config the service
before running, please make sure that the configuration will work at your side. you can config the service into ways.
- [defaults.py](.unit_conversion_api/defaults.py)
- enviroment varaible

if same config happen to be in both places. then the service will use the value in enviroment varaible

to understand how this service configuration works you can see this [config file](.unit_conversion_api/config.py)

#### Configurable Parameter
here's  the list of configuration and there uses;

- <b>SQLALCHEMY_DATABASE_URI</b>: URL string for the service to connect to
- <b>PORT</b>: port for the flask api will be running on for accepting rest api request
- <b>LOG_FILE_PATH</b>: location where the service will log to



### Running the service
if you using the virtual enviroment everything should be ready in bin folder

```
[path_to_desired_place]/venv/uniconver/bin/run-api
```

### Deployment process
1. clone this git
```
git clone [git_uri]
cd [git_uniconver]
```
    1.1 for production you can just use master
    1.2 for test environment please use develop branch
    ```
    git checkout develop
    
    ```
2. change directory into uniconver
```
cd uniconver
```

3. run postgres in docker (only it's not currently run)
```
source docker-psql.sh
```
4. create virtualenv
    4.1 if no virtual env install it
    ```
    pip3 install virtualenv
    ```
    4.2 make virtualenv
    ```
    virtualenv "$HOME"/uniconver
    ```
    4.3 activate virtualenv
    ```
    source "$HOME"/uniconver/bin/activate
    ```
    4.4 install requirement
    ```
    pip3 install -r requirements.txt
    ```
    4.5 install package
    pip3 install -e  .
5. run flask-migration to make sure the database has correct database model version
```
run-manage db upgrade
```

6. cp our supvisor run scritp into proper place
```
sudo cp uniconver.ini /etc/supervisord.d/
```
7. run the service with supervisor
```
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start
```

### test

#### Test Folder stucture
on test folder in will split into;
1. unit - for testing model (e.g. database Crud operation, testing logic function)
2. function - testing all API endpoint



#### run all test
```
python -m unittest discover
```

#### run single test
replace 'test_lentBook' with any filename you want to do the test
```
python -m unittest discover -p *test_lentBook.py
```

### API
Since there is still no swagger documentation yet. This section will be use to explain briefly about the all the API endpoint

#### monitior
this endpoint is to check if the database has been connect succesfully or not and which database is it currently connect

##### API Path
```
'/api/v1/monitor'
```
##### Response

```json
{
    "app": "unit_conversion_api",
    "client_ip": "127.0.0.1",
    "connection_pool": {
        "checked_in": 0,
        "checked_out": 0,
        "id": 140239944386400
    },
    "dependencies": {
        "engine": {
            "engine": "Engine(postgresql://unit_conversion_api_usr:***@127.0.0.1:5432/unit_conversion_api_dev)",
            "label": "<unknown>",
            "status": "OK",
            "time": "0.187"
        }
    },
    "node": "cpx-f8dmyvo1k2u",
    "proxy": null,
    "status": "OK"
}
```

#### unit
endpoint to make CRUD operation on unit e.g. add sqm unit

##### API Path
```
'/api/v1/units'
```

#### Query Parameter
- name: filter only unit with this name
- type: filter only unit with this type

#### Request Data
example request data to post new unit
```json
{
	"name":"sqm",
	"type":"area"
}
```

#### Response Data
endpoint to make CRUD operation on unit e.g. add sqm unit
```json
{
    "data": {
        "id": 143,
        "name": "sqm",
        "type": "area"
    },
    "message": "Created - entity created successfully",
    "status": "success",
    "status_code": 201
}
```

#### conversionFormulas
endpoint to make CRUD operation on unit conversion formula.
Currently the API only support unit conversion formula that can be represent in linear equation which should be cover majority of unit coversion use case.
e.g. area, weight, volume, time, temperature

##### API Path
```
'/api/v1/conversionFormulas'
```

#### Query Parameter
[TBD] still haven't been fully implemented

#### Request Data
example request data to post new unit
from linear equation y=(a1)*(x) + a0
Paremeter      | Data Type      | Detail
-------------  | -------------  | -------------
x_unit_id      | Integer        | Unit ID that will be convert
y_unit_id      | Integer        | Unit ID that will be convert into
a0             | String         | a0 in lineaer equation which is a string representation of floating number
a1             | String         | a1 in lineaer equation which is a string representation of floating number
```json
{
	"x_unit_id":143,
	"y_unit_id":144,
	"a0":"0",
	"a1":"0.000625"
	
}
```

#### Response Data
endpoint to make CRUD operation on unit e.g. add sqm unit
```json
{
    "data": {
        "a0": "0",
        "a1": "0.000625",
        "id": 26,
        "x_unit_id": 143,
        "y_unit_id": 144
    },
    "message": "Created - entity created successfully",
    "status": "success",
    "status_code": 201
}
```

#### conversion
endpoint with single method, POST. an endpoint to do actual unit converion


##### API Path
```
'/api/v1/conversion'
```

#### Request Data
example request to do conversion for sqm2rai

Paremeter      | Data Type      | Detail
-------------  | -------------  | -------------
x_unit_id      | Integer        | Unit ID that will be convert
y_unit_id      | Integer        | Unit ID that will be convert into
x_value        | String         | value of x unit that user want to convert which is a string representation of floating number

```json
{
	"x_value": "1600",
	"y_unit_id": 192,
	"x_unit_id": 191
	
}
```

#### Response Data

```json
{
    "y_value": "1",
	"x_value": "1600",
	"y_unit_id": 192,
	"x_unit_id": 191
	
}
```