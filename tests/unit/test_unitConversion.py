from operator import invert
import unittest
from unit_conversion_api.api.v1.resources.unitConversion import unitConversion

import math


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestUnitCoversionTests(unittest.TestCase):
    def setUp(self):
        # no setup need to be done
        pass

    def tearDown(self):
        # no tear down need to be done
        pass
    

    def test_convert_sqm2rai(self):
        """
        test convert sqm2rai and vise versa
        """

        data = {
            "a1":"0.000625",
            "a0":"0",
        }
        value = unitConversion("1600", data["a0"],data["a1"])
        self.assertTrue(math.isclose(value,1.0))

        #test formula invert formula
        value = unitConversion("0.2", data["a0"],data["a1"], isInvert=True)
        self.assertTrue(math.isclose(value,320))

    def test_convert_celsius2Fahrenheit(self):
        """
        test convert celsius2Fahrenheit and vise versa
        """
        data = {
            "a1":"1.8",
            "a0":"32",
        }
        value = unitConversion("10", data["a0"],data["a1"])
        self.assertTrue(math.isclose(value,50))

        #test formula invert formula
        value = unitConversion("14", data["a0"],data["a1"], isInvert=True)
        self.assertTrue(math.isclose(value,-10))
      

