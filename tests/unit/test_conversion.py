import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *
import math


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestConversionTests(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()
        
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err

    def tearDown(self):
        """Do the testing """
        emptyAllTable()
    

    def test_conversion(self):
        """
        try adding new conversion
        """
        try:

            data = {
                "a1":"0.000625",
                "a0":"0"
            }
            newCoversion = Conversion(**data)
            x_unit = Unit.query.filter_by(name="sqm").first()
            y_unit = Unit.query.filter_by(name="rai").first()
            newCoversion.x_unit_id =x_unit.id
            newCoversion.y_unit_id =y_unit.id
            db.session.add(newCoversion)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        
        #check if newUnit have been added into database sucessfully or not

        #check at unit table
        newCoversion = Conversion.query.filter_by(x_unit_id=x_unit.id, y_unit_id=y_unit.id).first()
        self.assertEqual(newCoversion.a1, data['a1'])
        self.assertEqual(newCoversion.a0, data['a0'])

