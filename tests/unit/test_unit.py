import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestUnitTests(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

    def tearDown(self):
        """Do the testing """
        print("tearing down after finnish the test case")
        emptyAllTable()

    def test_unit(self):
        """
        try adding new unit
        """
        try:

            data = {
                "name":"sqm",
                "type":"area"
            }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        
        #check if newUnit have been added into database sucessfully or not

        #check at unit table
        newUnit = Unit.query.filter_by(name=data['name']).first()
        self.assertEqual(newUnit.name, data['name'])
        self.assertEqual(newUnit.type, data['type'])

        #check at unitType table
        newUnit = UnitType.query.filter_by(name=data['type']).first()
        self.assertEqual(newUnit.name, data['type'])
        print("=== successfully run the test case ===")
