import unittest
from unit_conversion_api import create_app


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestCheckDb(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()


    def tearDown(self):
        """Do the testing """

    def test_checkDB(self):
        """
        test check if db can be connect successfully or not
        """

        response = self.client.get('/api/v1/monitor')
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(respData["app"], "unit_conversion_api")
        self.assertEqual(respData["status"], "OK")
   