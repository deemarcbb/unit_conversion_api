import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *
import math


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestPostExistFormula(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

        #add required unit
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            sqmUnit = Unit(**data)
            db.session.add(sqmUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            raiUnit = Unit(**data)
            db.session.add(raiUnit)
            #add second unit
            data = {
                    "name":"celcius",
                    "type":"temperature"
                }
            celsiusUnit = Unit(**data)
            db.session.add(celsiusUnit)
            #add minute unit
            data = {
                    "name":"fahrenheit",
                    "type":"temperature"
                }
            fahrenheitUnit = Unit(**data)
            db.session.add(fahrenheitUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err


        #add required formula
        try:
            #add sqm to rai
            data = {
                "a1":"0.000625",
                "a0":"0",
                "x_unit_id":sqmUnit.id,
                "y_unit_id":raiUnit.id,
            }
            sqm2rai = Conversion(**data)
            db.session.add(sqm2rai)
            # add min to sec
            data = {
                "a1":"1.8",
                "a0":"32",
                "x_unit_id":celsiusUnit.id,
                "y_unit_id":fahrenheitUnit.id,
            }
            celsius2Fahrenheit = Conversion(**data)
            db.session.add(celsius2Fahrenheit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        self.sqm2raiId = sqm2rai.id
        self.celsius2Fahrenheit = celsius2Fahrenheit.id
        self.sqmUnitId = sqmUnit.id
        self.raiUnitId = raiUnit.id
        self.celsiusUnitId = celsiusUnit.id
        self.fahrenheitUnitId = fahrenheitUnit.id

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_sqm2rai(self):
        """
        test covert sqm2rai
        """
        
        data = {
        "x_unit_id":self.sqmUnitId,
        "y_unit_id":self.raiUnitId,
        "x_value":"1600",
        }
  
        response = self.client.post(f'/api/v1/conversion', json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertTrue(math.isclose(float(respData["data"]["y_value"]), 1.0))

    def test_rai2sqm(self):
        """
        test covert rai2sqm
        """
        
        data = {
        "y_unit_id":self.sqmUnitId,
        "x_unit_id":self.raiUnitId,
        "x_value":"0.2",
        }
 
  
        response = self.client.post(f'/api/v1/conversion', json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertTrue(math.isclose(float(respData["data"]["y_value"]), 320))

    def test_celsius2Fahrenheit(self):
        """
        test covert celsius2Fahrenheit
        """
        
        data = {
        "x_unit_id":self.celsiusUnitId,
        "y_unit_id":self.fahrenheitUnitId,
        "x_value":"10",
        }
     
        response = self.client.post(f'/api/v1/conversion', json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertTrue(math.isclose(float(respData["data"]["y_value"]), 50))

    def test_fahrenheit2celsius(self):
        """
        test covert fahrenheit2celsius
        """
        
        data = {
        "y_unit_id":self.celsiusUnitId,
        "x_unit_id":self.fahrenheitUnitId,
        "x_value":"14",
        }
     
        response = self.client.post(f'/api/v1/conversion', json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertTrue(math.isclose(float(respData["data"]["y_value"]), -10))

    

