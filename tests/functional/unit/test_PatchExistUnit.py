import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestPatchExistUnit(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        self.dataId = newUnit.id
        self.dataType = newUnit.type

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_PatchExistUnit(self):
        """
        test exist unit from rai to sqm however sqm is already exist in database
        """

        data = {
                    "name":"sqm"
                }
        response = self.client.patch(f'/api/v1/units/{self.dataId}',json=data)
        logger.debug(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 409)
        respData = response.get_json()
        self.assertEqual(respData["message"], "Conflict")
  
   