import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestGetSingleType(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            #add second unit
            data = {
                    "name":"second",
                    "type":"time"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            #add minute unit
            data = {
                    "name":"minute",
                    "type":"time"
                }
            newUnit = Unit(**data)
            db.session.add(newUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_GetSingleType(self):
        """
        test get single type by param filtering
        """

        #check that currently there are 4 unit
        response = self.client.get(f'/api/v1/units')
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(len(respData["data"]), 4)

        #get only area type
        response = self.client.get(f'/api/v1/units?type=area')
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(len(respData["data"]), 2)

   