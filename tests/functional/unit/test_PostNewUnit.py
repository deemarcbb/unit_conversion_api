import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestPostNewUnit(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_PostNewUnit(self):
        """
        test add sqm unit
        """
        
        data = {
                    "name":"sqm",
                    "type":"area"
                }
        response = self.client.post('/api/v1/units',json=data)
        self.assertEqual(response.status_code, 201)
        respData = response.get_json()
        self.assertEqual(respData["data"]['name'], data['name'])
        self.assertEqual(respData["data"]['type'], data['type'])
   