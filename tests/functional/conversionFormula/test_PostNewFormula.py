import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestPostNewFormula(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            sqmUnit = Unit(**data)
            db.session.add(sqmUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            raiUnit = Unit(**data)
            db.session.add(raiUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        self.sqmUnitID = sqmUnit.id
        self.raiUnitID = raiUnit.id

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_PostNewFormula(self):
        """
        test add new formula to convert sqm to rai
        """
        
        data = {
                "a1":"0.000625",
                "a0":"0",
                "x_unit_id":self.sqmUnitID,
                "y_unit_id": self.raiUnitID
            }
        response = self.client.post('/api/v1/conversionFormulas',json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 201)
        respData = response.get_json()
        self.assertEqual(respData["data"]['a1'], data['a1'])
        self.assertEqual(respData["data"]['a0'], data['a0'])
        self.assertEqual(respData["data"]['x_unit_id'], data['x_unit_id'])
        self.assertEqual(respData["data"]['y_unit_id'], data['y_unit_id'])
   