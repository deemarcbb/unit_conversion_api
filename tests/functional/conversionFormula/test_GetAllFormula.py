import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestGetAllFormula(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

        #add required unit
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            sqmUnit = Unit(**data)
            db.session.add(sqmUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            raiUnit = Unit(**data)
            db.session.add(raiUnit)
            #add second unit
            data = {
                    "name":"second",
                    "type":"time"
                }
            secUnit = Unit(**data)
            db.session.add(secUnit)
            #add minute unit
            data = {
                    "name":"minute",
                    "type":"time"
                }
            minUnit = Unit(**data)
            db.session.add(minUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err


        #add required formula
        try:
            #add sqm to rai
            data = {
                "a1":"0.000625",
                "a0":"0",
                "x_unit_id":sqmUnit.id,
                "y_unit_id":raiUnit.id,
            }
            newFormula = Conversion(**data)
            db.session.add(newFormula)
            # add min to sec
            data = {
                "a1":"60",
                "a0":"0",
                "x_unit_id":minUnit.id,
                "y_unit_id":secUnit.id,
            }
            newFormula = Conversion(**data)
            db.session.add(newFormula)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_GetAllFormula(self):
        """
        test get all formula
        """
        
        response = self.client.get(f'/api/v1/conversionFormulas')
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(len(respData["data"]), 2)




   