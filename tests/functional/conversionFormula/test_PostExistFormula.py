import unittest
from unit_conversion_api import create_app
from unit_conversion_api.database import db, emptyAllTable
from unit_conversion_api.database.models import *


import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class TestPostExistFormula(unittest.TestCase):
    def setUp(self):
        app = create_app()
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        emptyAllTable()

        #add required unit
        try:
            #add sqm unit
            data = {
                    "name":"sqm",
                    "type":"area"
                }
            sqmUnit = Unit(**data)
            db.session.add(sqmUnit)
            #add rai unit
            data = {
                    "name":"rai",
                    "type":"area"
                }
            raiUnit = Unit(**data)
            db.session.add(raiUnit)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        self.sqmUnitID = sqmUnit.id
        self.raiUnitID = raiUnit.id

        #add required formula, sqm to rai
        try:
            data = {
                "a1":"0.000625",
                "a0":"0",
                "x_unit_id":sqmUnit.id,
                "y_unit_id":raiUnit.id,
            }
            newFormula = Conversion(**data)
            db.session.add(newFormula)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
        self.formulaId = newFormula.id
        self.formulaA0 = newFormula.a0
        self.formulaA1 = newFormula.a1

    def tearDown(self):
        """Do the testing """
        emptyAllTable()

    def test_PostExistFormula(self):
        """
        test add exist formula to convert sqm to rai
        """
        
        data = {
                "a1":"0.0007",
                "a0":"1",
                "x_unit_id":self.sqmUnitID,
                "y_unit_id": self.raiUnitID
            }
        response = self.client.post('/api/v1/conversionFormulas',json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(respData["data"]['id'], self.formulaId)
        self.assertEqual(respData["data"]['a1'], self.formulaA1)
        self.assertEqual(respData["data"]['a0'], self.formulaA0)
        self.assertEqual(respData["data"]['x_unit_id'], self.sqmUnitID)
        self.assertEqual(respData["data"]['y_unit_id'], self.raiUnitID)

    def test_PostExistInvertFormula(self):
        """
        test add exist invert formula to convert rai to sqm
        """
        
        data = {
                "a1":"1600",
                "a0":"0",
                "y_unit_id":self.sqmUnitID,
                "x_unit_id": self.raiUnitID
            }
        response = self.client.post('/api/v1/conversionFormulas',json=data)
        logger.info(f"rawResp: {response.data}")
        self.assertEqual(response.status_code, 200)
        respData = response.get_json()
        self.assertEqual(respData["data"]['id'], self.formulaId)
        self.assertEqual(respData["data"]['a1'], self.formulaA1)
        self.assertEqual(respData["data"]['a0'], self.formulaA0)
        self.assertEqual(respData["data"]['x_unit_id'], self.sqmUnitID)
        self.assertEqual(respData["data"]['y_unit_id'], self.raiUnitID)

   